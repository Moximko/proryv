﻿using System;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Resources/Config", menuName = "CreateConfig")]
public class Config : ScriptableObject {
	public List<RouteInfo> routeInfos;

	[Serializable]
	public class RouteInfo {
		public string category;
		public float timeInterval = 10;
		public float speed = 10;
		public int id;
		public int count;
	}
}
