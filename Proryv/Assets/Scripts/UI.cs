﻿using System.Linq;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI : MonoBehaviour {
	Dropdown categoriesList;
	Dispatcher dispatcher;
	public IEnumerable<string> categories {
		get {
			return categoriesList.options.Select(o => o.text);
		}
	}
	public GameObject stations;
	public GameObject transport;

	void Start() {
		categoriesList = GetComponentInChildren<Dropdown>();
	}
}
