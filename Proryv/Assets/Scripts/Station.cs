﻿using UnityEngine;
using UnityEngine.UI;

public class Station : MonoBehaviour {
	public float stayDuration = 30;
	public float stopDistance = 1;
	[SerializeField]
	Button iconPrefab;
	Button icon;

	void Start() {
		if (iconPrefab == null) {
			return;
		}
		var dispatcher = FindObjectOfType<Dispatcher>();
		var ui = FindObjectOfType<UI>();
		icon = Instantiate(iconPrefab);
		icon.transform.SetParent(ui.stations.transform);
		icon.onClick.AddListener(() => dispatcher.OnStationClicked(this));
	}
	void Update() {
		if (icon != null) {
			icon.transform.position = Camera.main.WorldToScreenPoint(transform.position);
		}
	}
}
