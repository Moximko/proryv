﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Route : MonoBehaviour {
	public int id;
	public List<Station> stations;
	public bool visualize {
		set {
			line.enabled = value;
		}
	}
	
	LineRenderer line;
	void Start() {
		line = GetComponent<LineRenderer>();
		line.positionCount = stations.Count;
		line.SetPositions(stations.Select(s => s.transform.position).ToArray());
	}
}
