﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class Transport : MonoBehaviour {
	public string category;
	public float timeInterval = 10;
	public int count = 5;
	public int id;
	public List<Station> stations;
	public bool visualize {
		set {
			icon.gameObject.SetActive(value);
		}
	}

	[SerializeField]
	Button iconPrefab;
	[SerializeField]
	float speed = 10;
	float stayTime;
	int targetIdx;
	Vector3 target;
	Button icon;

	public void Init(Route route) {
		stations = new List<Station>(route.stations);
		transform.position = stations.First().transform.position;
		transform.rotation = Quaternion.identity;
	}

	void Start() {
		target = stations[targetIdx].transform.position;

		var ui = FindObjectOfType<UI>();
		var dispatcher = FindObjectOfType<Dispatcher>();
		if (iconPrefab != null) {
			icon = Instantiate(iconPrefab);
			icon.transform.SetParent(ui.transport.transform);
			icon.onClick.AddListener(() => dispatcher.OnTransportClicked(this));
			icon.GetComponentInChildren<Text>().text = id.ToString();
		}
	}
	void Update() {
		UpdateIcon();
		var next = stations[targetIdx];
		var atTarget = Vector3.Distance(transform.position, next.transform.position) < next.stopDistance;
		if (!atTarget) {
			Go();
		} else if (stayTime == 0) {
			stayTime = next.stayDuration;
		} else {
			stayTime -= Time.deltaTime;
			if (stayTime <= 0) {
				stayTime = 0;
				OnStationServed();
			}
		}
	}
	void Go() {
		var dir = (target - transform.position).normalized;
		transform.position += dir * Time.deltaTime * speed;
	}
	void OnStationServed() {
		if (targetIdx == stations.Count - 1) {
			stations.Reverse();
			targetIdx = 0;
		}
		target = stations[++targetIdx].transform.position;
	}
	void UpdateIcon() {
		if (icon != null) {
			icon.transform.position = Camera.main.WorldToScreenPoint(transform.position);
		}
	}
}
