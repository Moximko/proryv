﻿using System.Collections.Generic;
using System.Linq;
using Assets.SimpleAndroidNotifications;
using UnityEngine;

public class Dispatcher : MonoBehaviour {
	[SerializeField]
	Config config;
	[SerializeField]
	List<Route> routes;
	[SerializeField]
	List<Transport> transportPrefabs;
	List<float> transportIntervals;
	List<int> transportCount;
	List<Transport> allTransport = new List<Transport>();
	List<string> categories = new List<string>();

	public IEnumerable<Transport> GetTransportFor(string category) {
		return GetAllTransport().Where(t => t.category == category);
	}
	public IEnumerable<Transport> GetTransportFor(Station station) {
		return GetAllTransport().Where(t => t.stations.Contains(station));
	}
	public IEnumerable<Route> GetRoutesFor(Station station) {
		return routes.Where(r => r.stations.Contains(station));
	}
	public IEnumerable<Transport> GetAllTransport() {
		return allTransport;
	}

	public void DrawCategory(string category) {
		categories.Add(category);
		OnCategoryChanged();
	}
	public void HideCategory(string category) {
		categories.Remove(category);
		OnCategoryChanged();
	}
	public void OnCategoryChanged() {
		OnRouteClicked();
		foreach (var transport in GetAllTransport()) {
			transport.visualize = categories.Contains(transport.category);
		}	
	}
	public void OnStationClicked(Station station) {
		foreach (var route in routes) {
			route.visualize = route.stations.Contains(station);
		}
	}
	public void OnTransportClicked(Transport transport) {
		foreach (var route in routes) {
			route.visualize = transport != null && route.id == transport.id;
		}
	}
	public void OnRouteClicked() {
		OnTransportClicked(null);
	}
	
	void Start() {
		routes = GetComponentsInChildren<Route>().ToList();
		transportIntervals = new List<float>();
		transportCount = new List<int>();
		foreach (var transport in transportPrefabs) {
			transportIntervals.Add(transport.timeInterval);
			transportCount.Add(transport.count);
		}

#if UNITY_ANDROID && !UNITY_EDITOR
        NotificationManager.SendBusNotification(10);
#endif

    }
	void Update() {
		for (var i = 0; i < transportIntervals.Count; ++i) {
			if (transportCount[i] == 0) {
				continue;
			}
			transportIntervals[i] -= Time.deltaTime;
			if (transportIntervals[i] <= 0) {
				var prefab = transportPrefabs[i];
				var route = routes.Single(r => r.id == prefab.id);
				var transport = Transport.Instantiate(prefab);
				transport.Init(route);
				allTransport.Add(transport);

				transportIntervals[i] = transport.timeInterval;
				--transportCount[i];
			}
		}
	}
}
