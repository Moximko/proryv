﻿using UnityEngine;
using UnityEngine.UI;

public class CategoryUI : MonoBehaviour {
	[SerializeField]
	string category;
	Dispatcher dispatcher;

	void Start() {
		dispatcher = FindObjectOfType<Dispatcher>();
		var toggle = GetComponent<Toggle>().isOn;
		OnCategoryChanged(toggle);
	}

	public void OnCategoryChanged(bool on) {
		if (on) {
			dispatcher.DrawCategory(category);
		} else {
			dispatcher.HideCategory(category);
		}
	}
}
