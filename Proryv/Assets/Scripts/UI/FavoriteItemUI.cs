﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Hackathon.UI
{
    public class FavoriteItemUI : MonoBehaviour
    {
        [SerializeField]
        Button alertButton;

        public static event Action AlertButtonClick;

        private void Start()
        {
            alertButton.onClick.AddListener(OnAlertButtonClicked);
        }

        private void OnAlertButtonClicked()
        {
            if (AlertButtonClick != null)
            {
                AlertButtonClick();
            }
        }
    }
}