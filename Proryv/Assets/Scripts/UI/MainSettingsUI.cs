﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Hackathon.UI
{
    public class MainSettingsUI : MonoBehaviour
    {
        [SerializeField]
        Button transportTypesButton;
        [SerializeField]
        Button searchRouteButton;
        [SerializeField]
        Button favoritesButton;
        [SerializeField]
        FavoritesUI favoritesUI;

        public event Action TransportTypesButtonClick; 

        private void Start()
        {
            transportTypesButton.onClick.AddListener(OnTransportTypesButtonClicked);
            searchRouteButton.onClick.AddListener(OnSearchButtonClicked);
            favoritesButton.onClick.AddListener(OnFavoritesButtonClicked);
        }

        private void OnTransportTypesButtonClicked()
        {

        }

        private void OnSearchButtonClicked()
        {

        }

        private void OnFavoritesButtonClicked()
        {
            favoritesButton.gameObject.SetActive(true);
        }
    }
}